const commonmark = require("commonmark");
const fs = require("fs");
const {sync: glob} = require("glob");
const sysPath = require("path");
const pug = require("pug");
const toml = require("toml");

const PROJECT_ROOT = __dirname;
const SRC_DIR = sysPath.join(PROJECT_ROOT, "src");
const HFEST_LEVELS_DIR = sysPath.join(SRC_DIR, "niveaux");
const HFEST_LEVEL_TEMPLATE = sysPath.join(SRC_DIR, "niveaux", "_niveau.pug");
const NIV_LEVEL_TEMPLATE = sysPath.join(SRC_DIR, "eternalfest", "_niveau.pug");
const DEDALE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/dedaleInfernal");
const NOUVELLECACHETTE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/nouvelleCachetteDuPlombier");
const JANVIER_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/janvier");
const FEVRIER_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/fevrier");
const MARS_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/mars");
const AVRIL_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/avril");
const MAI_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/mai");
const JUIN_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/juin");
const JUILLET_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/juillet");
const AOUT_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/aout");
const SEPTEMBRE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/septembre");
const OCTOBRE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/octobre");
const NOVEMBRE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/novembre");
const DECEMBRE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/niveauDuJour/decembre");
const ETERNOEL_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/eternoel");
const LANDESVIOLETTES_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/landesViolettes");
const LETERNUM_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/BlessedRacc/L'Eternum");
const SANCTUSDEORUM_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/BlessedRacc/Sanctus Deorum");
const CAVEINTERDITE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/darkkie/laCaveInterdite");
const ARENEGELULOZT_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/Simon/areneGelulozt");
const CRYPTEDANTOKHAMON_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/Simon/crypteDAntokhamon");
const MLAND_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/Simon/mLand");
const REPEREFRUITEUX_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations/Simon/repereFruiteux");
const CHUTELIBRE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/chuteLibre");
const TEMPLEDESABIMES_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/templeDesAbimes");
const HIMMELEN_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/himmelen");
const SOUSLACOLLINE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/sousLaColline");
const TEMPLEOUBLIE_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees/templeOublie");

const BUILD_DIR = sysPath.join(PROJECT_ROOT, "build");
const MD_PARSER = new commonmark.Parser();
const MD_RENDERER = new commonmark.HtmlRenderer();

function build() {
  copyAssets();
  renderHfestLevels();
  renderPugFiles();
  renderEternalfestLevels(DEDALE_LEVELS_DIR)
  renderEternalfestLevels(NOUVELLECACHETTE_LEVELS_DIR)
  renderEternalfestLevels(JANVIER_LEVELS_DIR)
  renderEternalfestLevels(FEVRIER_LEVELS_DIR)
  renderEternalfestLevels(MARS_LEVELS_DIR)
  renderEternalfestLevels(AVRIL_LEVELS_DIR)
  renderEternalfestLevels(MAI_LEVELS_DIR)
  renderEternalfestLevels(JUIN_LEVELS_DIR)
  renderEternalfestLevels(JUILLET_LEVELS_DIR)
  renderEternalfestLevels(AOUT_LEVELS_DIR)
  renderEternalfestLevels(SEPTEMBRE_LEVELS_DIR)
  renderEternalfestLevels(OCTOBRE_LEVELS_DIR)
  renderEternalfestLevels(NOVEMBRE_LEVELS_DIR)
  renderEternalfestLevels(DECEMBRE_LEVELS_DIR)
  renderEternalfestLevels(ETERNOEL_LEVELS_DIR)
  renderEternalfestLevels(LANDESVIOLETTES_LEVELS_DIR)
  renderEternalfestLevels(LETERNUM_LEVELS_DIR)
  renderEternalfestLevels(SANCTUSDEORUM_LEVELS_DIR)
  renderEternalfestLevels(CAVEINTERDITE_LEVELS_DIR)
  renderEternalfestLevels(ARENEGELULOZT_LEVELS_DIR)
  renderEternalfestLevels(CRYPTEDANTOKHAMON_LEVELS_DIR)
  renderEternalfestLevels(REPEREFRUITEUX_LEVELS_DIR)
  renderEternalfestLevels(CHUTELIBRE_LEVELS_DIR)
  renderEternalfestLevels(TEMPLEDESABIMES_LEVELS_DIR)
  renderEternalfestLevels(HIMMELEN_LEVELS_DIR)
  renderEternalfestLevels(SOUSLACOLLINE_LEVELS_DIR)
  renderEternalfestLevels(TEMPLEOUBLIE_LEVELS_DIR)
}

function copyAssets() {
  for (filePath of glob("/**/*.{png,svg,css}", {root: SRC_DIR, nodir: true})) {
    const outPath = sysPath.join(BUILD_DIR, sysPath.relative(SRC_DIR, filePath));
    copyFileSync(filePath, outPath);
  }
}

function renderPugFiles() {
  for (pugFile of getPugFilesFromDir(SRC_DIR)) {
    const relPath = sysPath.relative(SRC_DIR, pugFile);
    console.log(`Rendering ${relPath}`);
    const dataPath = pugFile.replace(/\.pug$/, ".json");
    const outPath = sysPath.join(BUILD_DIR, relPath).replace(/\.pug$/, ".html");
    const renderer = pug.compileFile(pugFile);
    const data = fs.existsSync(dataPath) ? require(dataPath) : {};
      data.imageExists = (imagePath) => {
        return fs.existsSync(sysPath.join(SRC_DIR, imagePath));
    }
    const html = renderer(data);
    outputFileSync(outPath, Buffer.from(html));
  }
}

function renderHfestLevels() {
  const renderer = pug.compileFile(HFEST_LEVEL_TEMPLATE);
  for (mdFile of getMdFilesFromDir(HFEST_LEVELS_DIR)) {
    const relPath = sysPath.relative(SRC_DIR, mdFile);
    console.log(`Rendering ${relPath}`);
    const outPath = sysPath.join(BUILD_DIR, relPath).replace(/\.md$/, ".html");
    const levelData = loadMdFile(mdFile);
    const html = renderer(levelData);
    outputFileSync(outPath, Buffer.from(html));
  }
}

function renderEternalfestLevels(land) {
  const renderer = pug.compileFile(NIV_LEVEL_TEMPLATE);
  for (mdFile of getMdFilesFromDir(land)) {
    const relPath = sysPath.relative(SRC_DIR, mdFile);
    console.log(`Rendering ${relPath}`);
    const outPath = sysPath.join(BUILD_DIR, relPath).replace(/\.md$/, ".html");
    const levelData = loadMdFile(mdFile);
    const html = renderer(levelData);
    outputFileSync(outPath, Buffer.from(html));
  }
}

function getPugFilesFromDir(dirPath) {
  return glob("/**/[!_]*.pug", {root: dirPath, nodir: true})
}

function getMdFilesFromDir(dirPath) {
  return glob("/**/[!_]*.md", {root: dirPath, nodir: true})
}

function loadMdFile(filePath) {
  const sourceText = fs.readFileSync(filePath, {encoding: "UTF-8"});
  const parsed = MD_PARSER.parse(sourceText);
  const data = {};
  if (parsed.firstChild !== null && parsed.firstChild.type === "code_block" && parsed.firstChild.info === "toml") {
    // The Markdown file starts with a TOML block of metadata
    const tomlText = parsed.firstChild.literal;
    parsed.firstChild.unlink();
    Object.assign(data, toml.parse(tomlText));
  }
  data.content = MD_RENDERER.render(parsed);
  return data;
}

function outputFileSync(filePath, data) {
  const dirPath = sysPath.dirname(filePath);
  fs.mkdirSync(dirPath, {recursive: true});
  fs.writeFileSync(filePath, data);
}

function copyFileSync(srcPath, destPath) {
  outputFileSync(destPath, fs.readFileSync(srcPath));
}

build();