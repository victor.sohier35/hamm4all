def laby(hauteur, nbHauteur, largeur, nbLargeur, chemin, niveau):
    for haut in range (hauteur):
        for larg in range (largeur):
            if haut == 0:
                nomFich = ''+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').md'
                fich = open(nomFich, 'w')
                fich.write('```toml\n')
                fich.write('title = "Niveau '+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+')"\n')
                fich.write('images = ["/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').png"]\n')
                fich.write('video = ""\n')
                fich.write('\n')
                fich.write('[nav]\n')
                fich.write('next = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur+1)+').html"\n')
                fich.write('dims = [{name = "< Niveau de gauche >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur-1)+';'+str(haut+nbHauteur)+').html"}, ')
                fich.write('{name = "< Niveau de droite >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur+1)+';'+str(haut+nbHauteur)+').html"}]\n')
                fich.write('```')
                fich.close()
            elif larg+nbLargeur == nbLargeur:
                nomFich = ''+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').md'
                fich = open(nomFich, 'w')
                fich.write('```toml\n')
                fich.write('title = "Niveau '+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+')"\n')
                fich.write('images = ["/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').png"]\n')
                fich.write('video = ""\n')
                fich.write('\n')
                fich.write('[nav]\n')
                fich.write('prev = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur-1)+').html"\n')
                fich.write('next = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur+1)+').html"\n')
                fich.write('dims = [{name = "< Niveau de droite >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur+1)+';'+str(haut+nbHauteur)+').html"}]\n')
                fich.write('```')
                fich.close()
            elif larg == largeur-1:
                nomFich = ''+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').md'
                fich = open(nomFich, 'w')
                fich.write('```toml\n')
                fich.write('title = "Niveau '+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+')"\n')
                fich.write('images = ["/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').png"]\n')
                fich.write('video = ""\n')
                fich.write('\n')
                fich.write('[nav]\n')
                fich.write('prev = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur-1)+').html"\n')
                fich.write('next = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur+1)+').html"\n')
                fich.write('dims = [{name = "< Niveau de gauche >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur-1)+';'+str(haut+nbHauteur)+').html"}]\n')
                fich.write('```')
                fich.close()
            elif haut == hauteur-1:
                nomFich = ''+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').md'
                fich = open(nomFich, 'w')
                fich.write('```toml\n')
                fich.write('title = "Niveau '+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+')"\n')
                fich.write('images = ["/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').png"]\n')
                fich.write('video = ""\n')
                fich.write('\n')
                fich.write('[nav]\n')
                fich.write('prev = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur-1)+').html"\n')
                fich.write('dims = [{name = "< Niveau de gauche >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur-1)+';'+str(haut+nbHauteur)+').html"}, ')
                fich.write('{name = "< Niveau de droite >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur+1)+';'+str(haut+nbHauteur)+').html"}]\n')
                fich.write('```')
                fich.close()
            else:
                nomFich = ''+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').md'
                fich = open(nomFich, 'w')
                fich.write('```toml\n')
                fich.write('title = "Niveau '+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+')"\n')
                fich.write('images = ["/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur)+').png"]\n')
                fich.write('video = ""\n')
                fich.write('\n')
                fich.write('[nav]\n')
                fich.write('prev = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur-1)+').html"\n')
                fich.write('next = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur)+';'+str(haut+nbHauteur+1)+').html"\n')
                fich.write('dims = [{name = "< Niveau de gauche >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur-1)+';'+str(haut+nbHauteur)+').html"}, ')
                fich.write('{name = "< Niveau de droite >", uri = "/'+chemin+'/'+niveau+'('+str(larg+nbLargeur+1)+';'+str(haut+nbHauteur)+').html"}]\n')
                fich.write('```')
                fich.close()
            
laby(10, 0, 22, -3, "eternalfest/contrees/templeOublie", "002.0.89.20.")
            
