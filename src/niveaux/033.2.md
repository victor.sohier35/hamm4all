```toml
title = "Niveau 33.2"
images = ["/niveaux/033.2.png"]
video = ""

[nav]
prev = "/niveaux/033.1.html"
next = "/niveaux/033.3.html"
dims = [{name = "< Niveau de gauche >", uri = "/niveaux/033.-1.html"}, {name = "< Niveau de droite >", uri = "/niveaux/033.0.2.html"}]
```
